//Tela inicial
//Mostra os arquivos de midia presentes no telefone e ao selecionar, chama a tela do player

package com.iot.mp3service.mp3service.ui;

import android.Manifest;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.IBinder;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.iot.mp3service.mp3service.R;
import com.iot.mp3service.mp3service.service.Mp3Service;
import com.iot.mp3service.mp3service.service.Mp3ServiceImpl;

public class MainActivity extends AppCompatActivity implements ServiceConnection, AdapterView.OnItemClickListener, android.support.v4.app.LoaderManager.LoaderCallbacks<Cursor> {

    private SimpleCursorAdapter mAdapter;
    private Mp3Service mMP3Service;

    String[] colunas = new String[]{
            MediaStore.MediaColumns.DISPLAY_NAME,
            MediaStore.MediaColumns.DATA,
            MediaStore.MediaColumns._ID
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        int[] componentes = new int[]{
                android.R.id.text1,
                android.R.id.text2
        };

        mAdapter = new SimpleCursorAdapter(
                this,
                android.R.layout.simple_list_item_2,
                null,
                colunas,
                componentes,
                0);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mAdapter.getCount() == 0) {
            String permissao = Manifest.permission.READ_EXTERNAL_STORAGE;
            if (ActivityCompat.checkSelfPermission(this, permissao) ==
                    PackageManager.PERMISSION_GRANTED) {
                getSupportLoaderManager().initLoader(0, null, this);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{permissao}, 0);
            }
        }

        Intent it = new Intent(this, Mp3ServiceImpl.class);
        startService(it);
        bindService(it, (ServiceConnection) this, BIND_AUTO_CREATE);

    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {

    }

    @Override
    public void onServiceDisconnected(ComponentName name) {

    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(
                this,
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                colunas,
                MediaStore.Audio.AudioColumns.IS_MUSIC +" = 1", //1 == true in selection
                null,
                null
        );    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        mAdapter.swapCursor(cursor);
        ListView listView = (ListView)findViewById(R.id.listView);
        listView.setAdapter(mAdapter);
        listView.setOnItemClickListener(this);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Cursor cursor = (Cursor)adapterView.getItemAtPosition(i);
        String musica = cursor.getString(
                cursor.getColumnIndex(MediaStore.Files.FileColumns.DATA));

        Intent it = new Intent(this, Mp3Activity.class);

        it.putExtra("MUSICA",musica);

       startActivity(it);

    }
}
