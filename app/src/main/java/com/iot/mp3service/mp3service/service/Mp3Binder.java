//Service Binder
package com.iot.mp3service.mp3service.service;

import android.os.Binder;

public class Mp3Binder extends Binder {
    private Mp3Service mServico;
    public Mp3Binder(Mp3Service s) {
        mServico = s;
    }
    public Mp3Service getServico() {
        return mServico;
    }
}
