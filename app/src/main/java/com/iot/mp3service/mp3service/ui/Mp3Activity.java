//Tela do Player
// Mostra o nome, progresso da música, tempo decorrido, volume e controles de transporte



package com.iot.mp3service.mp3service.ui;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;

import android.support.v7.app.AppCompatActivity;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.iot.mp3service.mp3service.R;
import com.iot.mp3service.mp3service.service.Mp3Binder;
import com.iot.mp3service.mp3service.service.Mp3Service;
import com.iot.mp3service.mp3service.service.Mp3ServiceImpl;




public class Mp3Activity extends AppCompatActivity
        implements ServiceConnection {


    private Mp3Service mMP3Service;
    private ProgressBar mPrgDuracao;
    private TextView mTxtMusica;
    private TextView mTxtDuracao;
    private String mMusica;
    private TextView volumeMusica;
    AudioManager audioManager;

    private Handler mHandler = new Handler();

    private Thread mThreadProgresso = new Thread() {
        public void run() {
            atualizarTela();
            if (mMP3Service.getTempoTotal() > mMP3Service.getTempoDecorrido()) {
                mHandler.postDelayed(this, 1000);
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mp3);
        mPrgDuracao = (ProgressBar) findViewById(R.id.progressBar);
        mTxtMusica = (TextView) findViewById(R.id.txtMusica);
        mTxtDuracao = (TextView) findViewById(R.id.txtTempo);
        volumeMusica = (TextView) findViewById(R.id.musicVolume);
    }


    @Override
    protected void onResume() {
        super.onResume();

        Intent it = new Intent(this, Mp3ServiceImpl.class);
        startService(it);
        bindService(it, this, BIND_AUTO_CREATE);
        }

    @Override
    protected void onPause() {
        super.onPause();
        unbindService(this);
        mHandler.removeCallbacks(mThreadProgresso);

    }

    public void btnPlayClick(View v) {
        mHandler.removeCallbacks(mThreadProgresso);
        if (mMusica != null) {
            mMP3Service.play(mMusica);
            mHandler.post(mThreadProgresso);
        }
    }

    public void btnPauseClick(View v) {
        mMP3Service.pause();
        mHandler.removeCallbacks(mThreadProgresso);
    }

    public void btnStopClick(View v) {
        mMP3Service.stop();
        mHandler.removeCallbacks(mThreadProgresso);
        mPrgDuracao.setProgress(0);
        mTxtDuracao.setText(DateUtils.formatElapsedTime(0));
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        mMP3Service = ((Mp3Binder) service).getServico();
        mHandler.post(mThreadProgresso);
        atualizarTela();

        Intent intent = getIntent();
        String mMusica = intent.getStringExtra("MUSICA");

        mHandler.removeCallbacks(mThreadProgresso);
        if (mMusica != null) {
            mMP3Service.play(mMusica);
            mHandler.post(mThreadProgresso);
        }

    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        mMP3Service = null;
    }

    private void atualizarTela() {
        // Mostra o valor do volume atual na tela
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        int MusicVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        volumeMusica.setText(" Volume da Música: " + MusicVolume);

        mMusica = mMP3Service.getMusicaAtual();
        mTxtMusica.setText(mMusica);
        mPrgDuracao.setMax(mMP3Service.getTempoTotal());
        mPrgDuracao.setProgress(mMP3Service.getTempoDecorrido());
        mTxtDuracao.setText(
                DateUtils.formatElapsedTime(mMP3Service.getTempoDecorrido() / 1000));
    }


}

