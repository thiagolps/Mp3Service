// interface do serviço MP3
//Será chamada na UI e será implementada na classe que proverá o serviço
package com.iot.mp3service.mp3service.service;

public interface Mp3Service {
    void play(String arquivo);
    void pause();
    void stop();
    String getMusicaAtual();
    int getTempoTotal();
    int getTempoDecorrido();
}
