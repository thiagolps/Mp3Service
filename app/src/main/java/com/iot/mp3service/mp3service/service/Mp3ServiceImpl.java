//Classe que impĺementa o serviço

package com.iot.mp3service.mp3service.service;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.app.NotificationCompat;
import android.widget.RemoteViews;

import com.iot.mp3service.mp3service.R;
import com.iot.mp3service.mp3service.ui.Mp3Activity;

import java.io.File;
import java.io.FileInputStream;


public class Mp3ServiceImpl extends Service implements Mp3Service, SensorEventListener {

    public static final String EXTRA_ARQUIVO = "arquivo";
    public static final String EXTRA_ACAO = "acao";
    public static final String ACAO_PLAY = "play";
    public static final String ACAO_PAUSE = "pause";
    public static final String ACAO_STOP = "stop";
    private MediaPlayer mPlayer;
    private String mArquivo;
    private boolean mPausado;

    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    AudioManager audioManager;
    int lastMusicVolume;
    int musicVolume;

    @Override
    public void onCreate() {
        super.onCreate();
        //Instancia o Media Player
        mPlayer = new MediaPlayer();
        // Seleciona o Acelerômetro para ler o valor através do Sensor manager
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        // Lê o volume atual da música através do Audiomanager
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        lastMusicVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);

    }



    @Override
    public IBinder onBind(Intent arg0) {
        return new Mp3Binder(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        if (intent != null) {
            if (ACAO_PLAY.equals(intent.getStringExtra(EXTRA_ACAO))) {
                play(intent.getStringExtra(EXTRA_ARQUIVO));
            } else if (ACAO_PAUSE.equals(intent.getStringExtra(EXTRA_ACAO))) {
                pause();
            } else if (ACAO_STOP.equals(intent.getStringExtra(EXTRA_ACAO))) {
                stop();
            }
        }
        return super.onStartCommand(intent, flags, startId);
    }

    // Implementação da interface Mp3Service
    @Override
    public void play(String musica) {
        if (musica != null && !mPlayer.isPlaying() && !mPausado) {
            try {
                mPlayer.reset();
                FileInputStream fis = new FileInputStream(musica);
                mPlayer.setDataSource(fis.getFD());
                mPlayer.prepare();
                mArquivo = musica;
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
        }
        mPausado = false;
        mPlayer.start();
        criarNotificacao();
    }

    @Override
    public void pause() {
        if (mPlayer.isPlaying()) {
            mPausado = true;
            mPlayer.pause();
        }
    }

    @Override
    public void stop() {
        if (mPlayer.isPlaying() || mPausado) {
            mPausado = false;
            mPlayer.stop();
            mPlayer.reset();

        }
        removerNotificacao();
        mSensorManager.unregisterListener(this);
    }

    @Override
    public String getMusicaAtual() {
        return mArquivo;
    }

    @Override
    public int getTempoTotal() {
        if (mPlayer.isPlaying() || mPausado) {
            return mPlayer.getDuration();
        }
        return 0;
    }

    @Override
    public int getTempoDecorrido() {
        if (mPlayer.isPlaying() || mPausado) {
            return mPlayer.getCurrentPosition();
        }
        return 0;
    }

    private void criarNotificacao() {
        Intent itPlay = new Intent(this, Mp3ServiceImpl.class);
        itPlay.putExtra(EXTRA_ACAO, ACAO_PLAY);
        Intent itPause = new Intent(this, Mp3ServiceImpl.class);
        itPause.putExtra(EXTRA_ACAO, ACAO_PAUSE);
        Intent itStop = new Intent(this, Mp3ServiceImpl.class);
        itStop.putExtra(EXTRA_ACAO, ACAO_STOP);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(Mp3Activity.class);
        stackBuilder.addNextIntent(new Intent(this, Mp3Activity.class));
        PendingIntent pitPlay = PendingIntent.getService(this, 1, itPlay, 0);
        PendingIntent pitPause = PendingIntent.getService(this, 2, itPause, 0);
        PendingIntent pitStop = PendingIntent.getService(this, 3, itStop, 0);
        PendingIntent pitActivity = stackBuilder.getPendingIntent(
                0, PendingIntent.FLAG_UPDATE_CURRENT);
        RemoteViews views = new RemoteViews(getPackageName(), R.layout.layout_notificacao);
        views.setOnClickPendingIntent(R.id.imgBtnPlay, pitPlay);
        views.setOnClickPendingIntent(R.id.imgBtnPause, pitPause);
        views.setOnClickPendingIntent(R.id.imgBtnClose, pitStop);
        views.setOnClickPendingIntent(R.id.txtMusica, pitActivity);
        views.setTextViewText(R.id.txtMusica,
                mArquivo.substring(mArquivo.lastIndexOf(File.separator) + 1));
        Notification n = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContent(views)
                .setOngoing(true)
                .build();
        NotificationManagerCompat nm = NotificationManagerCompat.from(this);
        nm.notify(1, n);
    }

    private void removerNotificacao() {
        NotificationManagerCompat nm = NotificationManagerCompat.from(this);
        nm.cancel(1);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        //Lê o valor do eixo Z do acelerômetro e seta o valor do volume através do ajuste por uma reta
        // Acelerômetro varia de -10 a 10 , Volume varia de 0 a 15
        Float z = event.values[2];
        Double reta = ((0.75 * z.intValue()) + 7.5);
        musicVolume = reta.intValue();

        //Se o valor ajustado lido pelo sensor for diferente do volume atual
        // Seta o valor atual de volume e assume o mesmo como último valor
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        if (musicVolume != lastMusicVolume) {
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, musicVolume, 1);
            lastMusicVolume = musicVolume;
        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
    public void onDestroy () {
        mSensorManager.unregisterListener(this);
    }
}
